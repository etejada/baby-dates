if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./service-worker.js').then(function() {
      console.log('Service Worker registrado con éxito');
    }).catch(function(error) {
      console.log('El registro del Service Worker falló:', error);
    });
  }

document.addEventListener('DOMContentLoaded', (event) => {   
    document.getElementById('reloadButton').addEventListener('click', function() {
        window.location.reload();
    });
    
    
    moment.locale('es');
    updateCurrentDate();
    
    const savedBirthDate = localStorage.getItem('birthdate');
    const savedExpectedDate = localStorage.getItem('babyExpectedDate');

    // Verificar si las fechas recuperadas son válidas
    const isBirthDateValid = moment(savedBirthDate).isValid();
    const isExpectedDateValid = moment(savedExpectedDate).isValid();

    if (!isBirthDateValid) {
        localStorage.removeItem('birthdate');
    }

    if (!isExpectedDateValid) {
        localStorage.removeItem('babyExpectedDate');
    }

    if (isBirthDateValid || isExpectedDateValid) {
        if (isBirthDateValid) {
            document.getElementById('birthdate').value = savedBirthDate;
        }

        if (isExpectedDateValid) {
            document.getElementById('babyExpectedDate').value = savedExpectedDate;
        }

        calculateAge();
        hideForm();
    } else {
        showForm();
    }
});

function updateCurrentDate() {
    const currentDateElement = document.getElementById('currentDate');
    currentDateElement.textContent = moment().format('LL');
}

function calculateAge() {
    const birthDateValue = document.getElementById('birthdate').value;
    const babyExpectedDateValue = document.getElementById('babyExpectedDate').value;
    const birthResultsBlock = document.getElementById('birthResultsBlock');
    const expectedResultsBlock = document.getElementById('expectedResultsBlock');
    
    if (birthDateValue) {
        const birthDate = moment(birthDateValue);
        document.getElementById('displayBirthDate').textContent = birthDate.format('LL');
        updateResults('FromBirth', moment().diff(birthDate, 'days'));
        birthResultsBlock.style.display = 'block';
    } else {
        document.getElementById('displayBirthDate').textContent = 'No proporcionado';
        clearResults('FromBirth');
        birthResultsBlock.style.display = 'none';
    }

    if (babyExpectedDateValue) {
        const babyExpectedDate = moment(babyExpectedDateValue);
        document.getElementById('displayExpectedDate').textContent = babyExpectedDate.format('LL');
        updateResults('FromExpected', moment().diff(babyExpectedDate, 'days'));
        expectedResultsBlock.style.display = 'block';
    } else {
        document.getElementById('displayExpectedDate').textContent = 'No proporcionado';
        clearResults('FromExpected');
        expectedResultsBlock.style.display = 'none';
    }

    localStorage.setItem('birthdate', birthDateValue);
    localStorage.setItem('babyExpectedDate', babyExpectedDateValue);
    
    hideForm();
}

function updateResults(suffix, totalDays) {
    const years = Math.floor(totalDays / 365.25);
    const months = Math.floor((totalDays % 365.25) / 30.44);
    const daysInMonths = Math.round((totalDays % 365.25) % 30.44);
    const weeks = Math.floor(totalDays / 7);
    const daysInWeeks = totalDays % 7; // días restantes después de contar las semanas completas

    document.getElementById(`weeks${suffix}`).textContent = `${weeks} semanas y ${daysInWeeks} días`;
    document.getElementById(`monthsAndDays${suffix}`).textContent = `${months} meses y ${daysInMonths} días`;
    document.getElementById(`yearsMonthsDays${suffix}`).textContent = `${years} años, ${months} meses y ${daysInMonths} días`;
}

function hideForm() {
    document.querySelector('.card').style.display = 'none';  // Ocultar formulario
    document.querySelector('.results-section').style.display = 'block';  // Mostrar resultados

    // Ocultar botones de editar
    const editButtons = document.querySelectorAll('.btn-secondary');
    editButtons.forEach(button => button.style.display = 'inline-block');
}

function showForm() {
    document.querySelector('.card').style.display = 'block';  // Mostrar formulario
    document.querySelector('.results-section').style.display = 'none';  // Ocultar resultados

    // Ocultar botones de editar
    const editButtons = document.querySelectorAll('.btn-secondary');
    editButtons.forEach(button => button.style.display = 'none');
}

function clearResults(suffix) {
    document.getElementById(`weeks${suffix}`).textContent = '-';
    document.getElementById(`monthsAndDays${suffix}`).textContent = '-';
    document.getElementById(`yearsMonthsDays${suffix}`).textContent = '-';
}