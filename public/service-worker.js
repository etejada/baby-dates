const CACHE_VERSION = 'v2';
const CURRENT_CACHES = {
    static: 'static-cache-' + CACHE_VERSION
};


self.addEventListener('install', function(event) {
  event.waitUntil(
      caches.open(CURRENT_CACHES.static).then(function(cache) {
          return cache.addAll([
              './index.html',
              './script.js',
              './styles.css',
          ]);
      })
  );
});

//eliminar todas las cachés antiguas que ya no son relevantes.
self.addEventListener('activate', (event) => {
  event.waitUntil(
      caches.keys().then((cacheNames) => {
          return Promise.all(
              cacheNames.map((cacheName) => {
                  if (cacheName !== CURRENT_CACHES.static) {
                      return caches.delete(cacheName);
                  }
              })
          );
      })
  );
});
  
self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});
